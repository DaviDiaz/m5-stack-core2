from m5stack import *
from m5stack_ui import *
from uiflow import *
import time
from easyIO import *

screen = M5Screen()
screen.clean_screen()
screen.set_screen_bg_color(0x292929)


start = None
mode = None
contador = None
mls = None
vibration = None


stop = M5Img("res/stop1-min.png", x=0, y=0, parent=None)
label3 = M5Label('0', x=143, y=86, color=0x000, font=FONT_MONT_48, parent=None)
label1 = M5Label('Start', x=96, y=160, color=0xFFFFFF, font=FONT_MONT_30, parent=None)
runing = M5Img("res/visual-effect-min.png", x=0, y=0, parent=None)
line0 = M5Line(x1=12, y1=203, x2=300, y2=203, color=0x000, width=2, parent=None)
bar0 = M5Bar(x=218, y=11, w=75, h=20, min=0, max=100, bg_c=0xa0a0a0, color=0x08A2B0, parent=None)
battery = M5Label('100', x=240, y=12, color=0x060505, font=FONT_MONT_18, parent=None)
turn_off = M5Btn(text='OFF', x=152, y=11, w=30, h=30, bg_c=0xffffff, text_c=0x824040, font=FONT_MONT_14, parent=None)
touch_button0 = M5Btn(text='FINISH', x=206, y=39, w=100, h=50, bg_c=0xFFFFFF, text_c=0x191919, font=FONT_MONT_22, parent=None)
label0 = M5Label('Add', x=227, y=212, color=0xffffff, font=FONT_MONT_24, parent=None)
label2 = M5Label('Less', x=40, y=212, color=0xffffff, font=FONT_MONT_24, parent=None)
ms = M5Label(':00', x=206, y=125, color=0x000, font=FONT_MONT_26, parent=None)


# Describe this function...
def Start():
  global start, mode, contador, mls, vibration
  runing.set_hidden(False)
  stop.set_hidden(True)
  while True:
    if contador != 0:
      while True:
        ms.set_text(str(mls))
        wait(1)
        mls = mls + 1
        if mls == 60:
          try :
            contador = contador - 1
            pass
          except:
            pass
          mls = 0
        label3.set_text(str(contador))
        if contador == 0:
          break
    else:
      speaker.playTone(889, 1)
      speaker.playTone(659, 1)
      speaker.playTone(698, 1)
      touch_button0.set_hidden(False)
      power.setVibrationIntensity(50)
      power.setVibrationEnable(True)
      runing.set_hidden(True)
      stop.set_hidden(False)
      label1.set_text('Start')
      break

# Describe this function...
def Clear():
  global start, mode, contador, mls, vibration
  power.setVibrationEnable(False)
  contador = 0
  mls = 0
  stop.set_hidden(False)
  label3.set_text(str(contador))
  ms.set_text(str(mls))

# Describe this function...
def ChangeStatus():
  global start, mode, contador, mls, vibration
  label1.set_text('Runing...')
  Start()

# Describe this function...
def More():
  global start, mode, contador, mls, vibration
  contador = contador + 5
  label3.set_text(str(contador))

# Describe this function...
def Less():
  global start, mode, contador, mls, vibration
  if contador < 0:
    contador = contador - 5
  else:
    contador = 5
  label3.set_text(str(contador))


def touch_button0_released():
  global start, mode, contador, mls, vibration
  touch_button0.set_hidden(True)
  Clear()
  pass
touch_button0.released(touch_button0_released)

def turn_off_pressed():
  global start, mode, contador, mls, vibration
  power.powerOff()
  pass
turn_off.pressed(turn_off_pressed)

def buttonA_wasPressed():
  global start, mode, contador, mls, vibration
  mode = 1
  speaker.playTone(330, 1/2)
  pass
btnA.wasPressed(buttonA_wasPressed)

def buttonB_wasPressed():
  global start, mode, contador, mls, vibration
  mode = 2
  speaker.playTone(494, 1/2)
  pass
btnB.wasPressed(buttonB_wasPressed)

def buttonC_wasPressed():
  global start, mode, contador, mls, vibration
  mode = 3
  speaker.playTone(392, 1/2)
  pass
btnC.wasPressed(buttonC_wasPressed)


Clear()
lv.obj.set_click(lv.scr_act(), False)
mode = 0
vibration = False
contador = 0
mls = 0
touch_button0.set_hidden(True)
power.setVibrationEnable(False)
runing.set_hidden(True)
stop.set_hidden(False)
screen.set_screen_brightness(30)
while True:
  bar0.set_value((map_value((power.getBatVoltage()), 3.7, 4.1, 0, 100)))
  battery.set_text(str(map_value((power.getBatVoltage()), 3.7, 4.1, 0, 100)))
  if mode == 1:
    Less()
  elif mode == 2:
    ChangeStatus()
  elif mode == 3:
    More()
  mode = 0
  wait_ms(2)
