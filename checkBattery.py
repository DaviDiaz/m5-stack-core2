from m5stack import *
from m5stack_ui import *
from uiflow import *
from easyIO import *
import time

screen = M5Screen()
screen.clean_screen()
screen.set_screen_bg_color(0x9c9c9c)





label0 = M5Label('Color', x=13, y=25, color=0xFFFFFF, font=FONT_MONT_28, parent=None)
label2 = M5Label('Text', x=14, y=147, color=0xffffff, font=FONT_MONT_28, parent=None)
label1 = M5Label('Size:5', x=9, y=87, color=0xFFFFFF, font=FONT_MONT_28, parent=None)
label3 = M5Label('Text', x=190, y=25, color=0xffffff, font=FONT_MONT_28, parent=None)
label4 = M5Label('Text', x=190, y=87, color=0xffffff, font=FONT_MONT_28, parent=None)
label5 = M5Label('Text', x=190, y=147, color=0xffffff, font=FONT_MONT_28, parent=None)


power.setChargeCurrent(power.CURRENT_190MA)
while True:
  label0.set_text(str(power.getVBusVoltage()))
  label1.set_text(str(power.getBatVoltage()))
  label2.set_text(str(map_value((power.getBatVoltage()), 3.7, 4.1, 0, 100)))
  label3.set_text(str(power.getTempInAXP192()))
  label4.set_text(str(power.getChargeState()))
  label5.set_text(str(power.getBatCurrent()))
  wait(1)
  if not (power.getChargeState()):
    power.powerOff()
  wait_ms(2)
